[![pipeline status](https://git.jems-group.com/devops-community/kubernetes/k8s-sandbox-playbook/badges/master/pipeline.svg)](https://git.jems-group.com/devops-community/kubernetes/k8s-sandbox-playbook/-/commits/master)

# Kubernetes Ansible Playbook for k8s-sandbox
  **requirements**: 
    ansible-galaxy install -r requirements.yaml 
  **update-requirements**: 
    ansible-galaxy install -r requirements.yaml --force

- si on desire un master il faudra definir la variable : 
    vars:
    kubernetes_role: "master"
- si c'est worker faudra changer master par node 
 **lancer la commande** : 
    ansible-playbook -i hosts **(inventory)** playbooks.yaml
**ajouter** --ask-become-pass et votre mot de pass  
